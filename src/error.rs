//! Error types

use std::io;

use super::index::LeafRange;
use super::model::Bytes;
use super::Node;

/// Failure when creatin HashTree
#[derive(Debug, Fail)]
pub enum CreationError {
    /// Provided storage has invalid size
    #[fail(display = "Size of provided storage is invalid for TreeStorage")]
    InvalidSize,

    /// Root of storage does not match expected hash
    #[fail(display = "Root hash in previous storage does not match expected")]
    InvalidRoot,

    /// IO failure
    #[fail(display = "IO Error: {}", _0)]
    IO(#[cause] io::Error),
}

impl PartialEq for CreationError {
    fn eq(&self, rhs: &Self) -> bool {
        format!("{:?}", self) == format!("{:?}", rhs)
    }
}

impl From<io::Error> for CreationError {
    fn from(e: io::Error) -> Self {
        CreationError::IO(e)
    }
}

/// Failure when operating on a HashTree
#[derive(Debug, Fail)]
pub enum TreeError {
    /// IO failure
    #[fail(display = "IO Error: {}", _0)]
    IO(#[cause] io::Error),

    /// Requested Index is outside the Tree
    #[fail(display = "Requested Index {:?} is outside the Tree", _0)]
    IndexOverflow(Node),

    /// Tree is missing data for requested Node
    #[fail(display = "Tree is missing data for {:?} and will need proof to cover {:?}", _0, _1)]
    Missing(LeafRange, LeafRange),

    /// Given patch is invalid
    #[fail(display = "Invalid patch: {}", _0)]
    InvalidPatch(#[cause] PatchError),

    /// Conflict when attempting to write a hash. Other hash were already written
    #[fail(display = "Conflicting hashes at {:?} (old: {:?} new: {:?}", _0, _1, _2)]
    HashConflict(Node, Bytes, Bytes),
}

impl PartialEq for TreeError {
    fn eq(&self, rhs: &Self) -> bool {
        format!("{:?}", self) == format!("{:?}", rhs)
    }
}

impl From<PatchError> for TreeError {
    fn from(e: PatchError) -> Self {
        TreeError::InvalidPatch(e)
    }
}

impl From<io::Error> for TreeError {
    fn from(e: io::Error) -> Self {
        TreeError::IO(e)
    }
}

impl Into<io::Error> for TreeError {
    fn into(self) -> io::Error {
        match self {
            TreeError::IO(e) => e,
            err => panic!("Tried to coerce non-IO-error {:?}", err),
        }
    }
}

/// Failure when merging a given patch
#[derive(Debug, Fail, PartialEq)]
pub enum PatchError {
    /// Patch is internally contradictionary in node
    #[fail(display = "Patch is internally contradictionary in node {:?}", _0)]
    Internal(Node),

    /// Patch is not attached to the given tree
    #[fail(display = "Patch attachment-violation {:?}", _0)]
    Attachment(Node),

    /// Patch is missing required proof
    #[fail(display = "Patch is missing required proof {:?}", _0)]
    Incomplete(Node),
}
