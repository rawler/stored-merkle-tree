use super::index::{Branch, Index, Leaf, Node};

use std::cmp::min;

pub trait TreeLayout {
    fn is_inside(&self, idx: &Node) -> bool;
    fn effective_node(&self, idx: &Node) -> Node;
    fn effective_sibling(&self, idx: &Node) -> Option<Node>;
    fn effective_parent(&self, idx: &Node) -> Option<Branch>;
    fn effective_sibling_and_parent(&self, idx: &Node) -> Option<(Node, Branch)>;

    // Right bound is just right of the rightmost_descendant()
    // That is
    fn right_bound(&self, idx: &Node) -> Leaf;
}

#[derive(Debug, Clone, PartialEq, PartialOrd, Eq, Ord)]
pub struct BoundTree {
    pub leaves: usize,
}

impl BoundTree {
    pub fn last_leaf(&self) -> Option<Leaf> {
        match self.leaves {
            0 => None,
            i => Some(Leaf(i - 1)),
        }
    }

    pub fn root(&self) -> Option<Node> {
        self.last_leaf().map(|n| n.root())
    }
}

/// Logic for navigating a (non-full?) tree
impl TreeLayout for BoundTree {
    fn is_inside(&self, idx: &Node) -> bool {
        match self.leaves {
            0 => false,
            b => *idx <= Node::leaf(b - 1),
        }
    }

    fn effective_node(&self, idx: &Node) -> Node {
        let mut current = *idx;
        while !self.is_inside(&current) {
            current = match current {
                Node::Branch(i) => Branch(i).left_child(),
                Node::Leaf(_) => panic!("Invalid index {:?}", idx),
            }
        }
        current
    }

    fn effective_sibling(&self, idx: &Node) -> Option<Node> {
        let sibling = idx.sibling();
        let parent = idx.parent().to_node();
        if self.is_inside(&parent) {
            return Some(self.effective_node(&sibling));
        }
        match self.root() {
            Some(root) if root.level() > idx.level() => self.effective_sibling(&parent),
            _ => None,
        }
    }

    fn effective_parent(&self, idx: &Node) -> Option<Branch> {
        let parent = idx.parent();
        let parent_node = parent.to_node();
        if self.is_inside(&parent_node) {
            return Some(parent);
        }
        match self.root() {
            Some(root) if root.level() > idx.level() => self.effective_parent(&parent_node),
            _ => None,
        }
    }

    fn effective_sibling_and_parent(&self, idx: &Node) -> Option<(Node, Branch)> {
        self.effective_sibling(idx)
            .map(|sib| (sib, self.effective_parent(idx).unwrap()))
    }

    fn right_bound(&self, idx: &Node) -> Leaf {
        min(idx.rightmost_descendant().next(), Leaf(self.leaves))
    }
}
