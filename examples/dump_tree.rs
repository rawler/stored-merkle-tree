extern crate stored_merkle_tree;

use std::io::{stdin, Error};
use stored_merkle_tree::{FullLayout, SHA256_STANDARD, SimpleSetup};

pub fn to_hex_string(bytes: Vec<u8>) -> String {
    let strs: Vec<String> = bytes.iter().map(|b| format!("{:02X}", b)).collect();
    strs.join("")
}

fn main() -> Result<(), Error> {
    let stdin = stdin();
    let builder = FullLayout::make_builder(&SHA256_STANDARD, Vec::new());
    let tree = builder.consume_and_finish(stdin.lock())?;

    println!("{:?}", tree.dump());

    Ok(())
}
