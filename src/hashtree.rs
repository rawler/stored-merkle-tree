use super::error::{CreationError, PatchError, TreeError};
use super::hasher::Hasher;
use super::index::{Index, LeafRange, Nodes};
use super::io::BlockBuffer;
use super::model::{Bytes, ExtendableNodeWriter, NodeContent, NodeWriter};
use super::patch::Proof;
use super::tree::{BoundTree, TreeLayout};
use super::*;

use std;
use std::cmp::min;
use std::io;

/// Core structure of the Crate. Central type for all manipulations (extract and)
/// apply patches of a Tree
pub struct HashTree<H: Hasher, TS: HashReader<HashValue = H::Output> + SizedTree> {
    pub(crate) hasher: H,
    pub(crate) tree: BoundTree,
    pub(crate) storage: TS,
}

impl<H: Hasher, TS: HashReader<HashValue = H::Output> + SizedTree> HashTree<H, TS> {
    pub fn new(hasher: &H, storage: TS) -> Result<Self, CreationError>
    where
        TS: SizedTree,
    {
        Self::validate_root(&storage, &storage.tree())?;
        Ok(Self {
            hasher: hasher.clone(),
            tree: storage.tree(),
            storage,
        })
    }

    fn validate_root(storage: &TS, tree: &BoundTree) -> Result<(), CreationError> {
        match &tree.root() {
            None => Ok(()),
            Some(root) => match storage.read_hash(&root)? {
                Some(_) => Ok(()),
                None => Err(CreationError::InvalidRoot),
            },
        }
    }

    pub fn new_for(hasher: &H, mut storage: TS, root: &TS::HashValue) -> Result<Self, CreationError>
    where
        TS: NodeWriter + SizedTree,
    {
        let root_idx = storage.tree().root().ok_or(CreationError::InvalidSize)?;
        let root = NodeContent::rumored(root);
        if root
            .is_update_over(storage.read_hash(&root_idx)?)
            .map_err(|_| CreationError::InvalidRoot)?
        {
            storage.write_node(&root_idx, &root)?;
        }
        Self::new(hasher, storage)
    }

    pub fn tree(&self) -> &BoundTree {
        &self.tree
    }

    pub fn unwrap_storage(self) -> TS {
        self.storage
    }

    pub fn write(&mut self, blk_index: usize, data: &[u8]) -> Result<(), TreeError>
    where
        TS: NodeWriter,
    {
        let bs = self.hasher.block_size();
        for (i, block) in data.chunks(bs).enumerate() {
            self.write_block(Leaf(blk_index + i), block)?
        }
        Ok(())
    }

    pub fn root_hash(&self) -> Result<NodeContent<TS::HashValue>, TreeError> {
        match self.tree.root() {
            None => Ok(self.hasher.leaf_node(&b""[..])),
            Some(root) => Ok(self.storage.read_hash(&root)?.unwrap()),
        }
    }

    pub fn patch_for(&self, range: LeafRange, proof_for: LeafRange) -> Result<Patch<H>, TreeError>
    where
        TS: DataReader,
    {
        let blocks = self.tree.leaves;
        if range.end.0 > blocks {
            return Err(TreeError::IndexOverflow(range.start.to_node()));
        }

        let range = Leaf::range(range.start.0..min(range.end.0, blocks));
        let data = self.read_data_blocks(range.clone())?;
        let proof = self.read_proof(range.clone(), proof_for)?;

        Ok(proof.with_data(self.hasher.split(data).with_offset(range.start.0)))
    }

    pub fn dump(&self) -> TreeDumper<TS>
    where
        TS: TreeStorage,
    {
        self.storage.dump()
    }

    fn read_data_blocks(&self, indices: LeafRange) -> Result<Bytes, TreeError>
    where
        TS: DataReader,
    {
        self.check_missing(&indices)?;
        let mut res =
            Vec::with_capacity(self.hasher.block_size() * (indices.end.0 - indices.start.0));
        for i in indices.start.0..indices.end.0 {
            let i = Leaf(i);
            let block = self.storage.read_data(&i)?.expect("Expected data missing");
            res.extend_from_slice(&block);
        }
        Ok(res.into())
    }

    fn get_first_and_last_result_matching<Item, Err, Iter, Cond>(
        mut i: Iter,
        cond: Cond,
    ) -> Result<Option<(Item, Item)>, Err>
    where
        Item: Clone,
        Iter: Iterator<Item = Item>,
        Cond: Fn(&Item) -> Result<bool, Err>,
    {
        let first = loop {
            match i.next() {
                None => return Ok(None),
                Some(item) => {
                    if cond(&item)? {
                        break item;
                    }
                }
            }
        };

        let mut last = first.clone();
        for item in i {
            if cond(&item)? {
                last = item
            } else {
                break;
            }
        }
        Ok(Some((first, last)))
    }

    fn check_missing(&self, data_range: &LeafRange) -> Result<(), TreeError> {
        let roots = data_range.roots();

        match Self::get_first_and_last_result_matching(roots.into_iter(), |idx| {
            self.storage
                .read_hash(&idx)
                .map(|got| got.map(|n| n.is_filled) != Some(true))
        })? {
            None => Ok(()),
            Some((first, last)) => {
                let missing = first.leftmost_descendant()..self.tree.right_bound(&last);
                Err(TreeError::Missing(
                    missing.clone(),
                    self.required_proof(missing)?,
                ))
            }
        }
    }

    fn read_proof(&self, data_range: LeafRange, horizon: LeafRange) -> Result<Proof<H>, io::Error> {
        data_range
            .proofs(horizon)
            .into_iter()
            .map(|idx| self.read_expected_hash(&idx))
            .collect()
    }

    fn required_proof(&self, data_range: LeafRange) -> Result<LeafRange, io::Error> {
        let roots = data_range.roots();
        let (left_top, right_top) = match roots.len() {
            0 => return Ok(data_range.clone()),
            1 => {
                let top = self.first_proven_ancestor(*roots.first().unwrap())?;
                (top, top)
            }
            _ => {
                let left = self.first_proven_ancestor(*roots.first().unwrap())?;
                let right = self.first_proven_ancestor(*roots.last().unwrap())?;
                (left, right)
            }
        };
        Ok(left_top.leftmost_descendant()..self.tree.right_bound(&right_top))
    }

    fn first_proven_ancestor(&self, node: Node) -> Result<Node, io::Error> {
        assert!(self.tree.is_inside(&node));
        match self.storage.read_hash(&node)? {
            Some(_) => Ok(node),
            None => {
                let parent = self
                    .tree
                    .effective_parent(&node)
                    .expect("No parent had a hash. Not even root")
                    .to_node();
                self.first_proven_ancestor(parent)
            }
        }
    }

    fn read_expected_hash(&self, idx: &Node) -> Result<(Node, H::Output), io::Error> {
        let node_content = self
            .storage
            .read_hash(&idx)?
            .expect("Expected hash missing");
        Ok((*idx, node_content.hash))
    }

    fn pushup_hash(&mut self, idx: Node, hash: NodeContent<H::Output>) -> Result<(), TreeError>
    where
        TS: NodeWriter,
    {
        let (sib, parent) = match self.tree.effective_sibling_and_parent(&idx) {
            Some((sib, parent)) => (sib, parent.to_node()),
            None => return Ok(()),
        };

        if let Some(sib_hash) = self.storage.read_hash(&sib)? {
            let parent_hash = self
                .hasher
                .merge_tree_unsorted((&sib, &sib_hash), (&idx, &hash));
            if self.storage.change(&parent, &parent_hash)? {
                return self.pushup_hash(parent, parent_hash);
            }
        }

        Ok(())
    }

    fn write_block(&mut self, index: Leaf, data: &[u8]) -> Result<(), TreeError>
    where
        TS: NodeWriter,
    {
        let hash_node = self.hasher.leaf_node(data);
        self.storage
            .write_leaf(&index, &hash_node.hash, data)
            .map_err(Into::into)
            .and_then(|_| self.pushup_hash(index.to_node(), hash_node))
    }

    pub fn merge(&mut self, p: Patch<H>) -> Result<(), TreeError>
    where
        TS: NodeWriter,
    {
        let verified = p.inflate(&self.hasher, &self.tree)?;

        for (idx, value) in verified.proof.roots() {
            if !self.verify_attachment(idx, value)? {
                return Err(TreeError::InvalidPatch(PatchError::Attachment(*idx)));
            }
        }

        for (i, b) in verified.data.iter() {
            self.storage.write_data(&Leaf(i), b)?;
        }
        for (idx, content) in verified.proof.iter() {
            if self.storage.change(&idx, &content)? && content.is_filled
                && !self
                    .tree
                    .effective_parent(idx)
                    .map(|parent_idx| verified.proof.is_filled(parent_idx.to_node()))
                    .unwrap_or(false)
            {
                self.pushup_hash(*idx, *content)?;
            }
        }

        Ok(())
    }

    fn verify_attachment(
        &self,
        idx: &Node,
        hash_value: &NodeContent<H::Output>,
    ) -> Result<bool, TreeError> {
        if !self.tree.is_inside(idx) {
            return Ok(false);
        }
        if let Some(NodeContent { hash: existing, .. }) = self.storage.read_hash(idx)? {
            return Ok(existing == hash_value.hash);
        }
        let (sibling, parent) = self.tree.effective_sibling_and_parent(idx).unwrap();

        if let Some(sibling_hash) = self.storage.read_hash(&sibling)? {
            let parent_hash = self
                .hasher
                .merge_tree_unsorted((idx, hash_value), (&sibling, &sibling_hash));
            let stored_parent = self
                .storage
                .read_hash(&parent.to_node())?
                .expect("Sibling without parent");
            return Ok(parent_hash.hash == stored_parent.hash);
        }
        Ok(false)
    }
}

pub struct Builder<H: Hasher, TS: ExtendableNodeWriter<HashValue = H::Output>> {
    hasher: H,
    block: usize,
    storage: TS,
}

/// Helper structure for linearly building a HashTree from a datastream
pub struct BufferedBuilder<H: Hasher, TS: ExtendableNodeWriter<HashValue = H::Output>> {
    builder: Builder<H, TS>,
    buffer: BlockBuffer,
}

impl<H: Hasher, TS: ExtendableNodeWriter<HashValue = H::Output>> Builder<H, TS> {
    pub fn new(hasher: &H, storage: TS) -> Self {
        Self {
            hasher: hasher.clone(),
            block: 0,
            storage,
        }
    }

    fn pushup_hash(&mut self, idx: Node, hash: NodeContent<H::Output>) -> Result<(), TreeError> {
        let sib = idx.sibling();
        if sib > idx {
            return Ok(());
        }

        if let Some(sib_hash) = self.storage.read_hash(&sib)? {
            let parent = idx.parent().to_node();
            let parent_hash = self.hasher.merge_tree(&sib_hash, &hash);
            if self.storage.change(&parent, &parent_hash)? {
                return self.pushup_hash(parent, parent_hash);
            }
        }

        Ok(())
    }

    pub fn push_leaf(&mut self, block: &[u8]) -> io::Result<()> {
        assert!(block.len() <= self.hasher.block_size());
        let idx = Leaf(self.block);
        let hash_node = self.hasher.leaf_node(block);
        self.storage.write_leaf(&idx, &hash_node.hash, block)?;
        self.pushup_hash(idx.to_node(), hash_node)
            .map_err(Into::<io::Error>::into)?;
        self.block += 1;
        Ok(())
    }

    pub fn finish(mut self) -> io::Result<HashTree<H, TS::Sized>> {
        let mut roots = Leaf::range(0..self.block).roots().into_iter().rev();
        if let Some(tail) = roots.next() {
            let mut right_hash = self.storage.read_hash(&tail)?.unwrap();
            for tree_root in roots {
                let left_hash = self.storage.read_hash(&tree_root)?.unwrap();
                let merged = self.hasher.merge_tree(&left_hash, &right_hash);
                self.storage
                    .write_node(&tree_root.parent().to_node(), &merged)?;
                right_hash = merged;
            }
        }

        let storage = self.storage.try_into_sized().expect("Readable");
        Ok(HashTree::new(&self.hasher, storage).expect("Tree created"))
    }
}

impl<H: Hasher, TS: ExtendableNodeWriter<HashValue = H::Output>> BufferedBuilder<H, TS> {
    pub fn new(hasher: &H, storage: TS) -> Self {
        Self {
            builder: Builder::new(hasher, storage),
            buffer: BlockBuffer::new(hasher.block_size()),
        }
    }

    pub fn consume_and_finish<R: io::Read>(
        mut self,
        data: R,
    ) -> io::Result<HashTree<H, TS::Sized>> {
        self.consume_reader(data)?;
        self.finish()
    }

    pub fn consume_reader<R: io::Read>(&mut self, data: R) -> io::Result<()> {
        let builder = &mut self.builder;
        self.buffer
            .consume_reader(data, |block| builder.push_leaf(block))
    }

    pub fn consume_slice(&mut self, data: &[u8]) -> io::Result<()> {
        let builder = &mut self.builder;
        self.buffer
            .consume_slice(data, |block| builder.push_leaf(block))
    }

    pub fn finish(mut self) -> io::Result<HashTree<H, TS::Sized>> {
        {
            let builder = &mut self.builder;
            self.buffer.flush(|block| builder.push_leaf(block))?;
        }
        self.builder.finish()
    }
}

/// Debug-tool. Helper to render a Tree into a string
pub struct TreeDumper<'a, TS: 'a + HashReader + DataReader + SizedTree> {
    storage: &'a TS,
    show_data: bool,
}

impl<'a, TS> TreeDumper<'a, TS>
where
    TS: HashReader + DataReader + SizedTree,
{
    pub fn new(storage: &'a TS, show_data: bool) -> Self {
        Self { storage, show_data }
    }

    fn data_hex(d: Option<model::Bytes>) -> String {
        match d {
            Some(data) => to_hex_string(data),
            None => "None".to_owned(),
        }
    }

    fn fmt_inner(
        &self,
        f: &mut fmt::Formatter,
        idx: Node,
        indent: usize,
    ) -> std::result::Result<(), fmt::Error> {
        let hv = self.storage.read_hash(&idx).or(Err(fmt::Error))?;
        let spaces = || "  ".repeat(indent);

        let write_hv = |f: &mut fmt::Formatter| match hv {
            Some(hv) => writeln!(f, "{}{:?} {:x}", spaces(), idx, hv),
            None => writeln!(f, "{}{:?} Unknown", spaces(), idx),
        };

        match idx {
            Node::Branch(i) => {
                let left_child = Branch(i).left_child();
                let right_child = self.storage.tree().effective_sibling(&left_child).unwrap();
                self.fmt_inner(f, left_child, indent + 1)?;
                write_hv(f)?;
                self.fmt_inner(f, right_child, indent + 1)
            }
            Node::Leaf(i) => {
                write_hv(f)?;
                if self.show_data && hv.is_some() {
                    let data = self.storage.read_data(&Leaf(i)).or(Err(fmt::Error))?;
                    writeln!(
                        f,
                        "{}Data: {:?}\n",
                        "   ".repeat(indent + 1),
                        Self::data_hex(data)
                    )?;
                }
                Ok(())
            }
        }
    }
}

impl<'a, TS> Debug for TreeDumper<'a, TS>
where
    TS: HashReader + DataReader + SizedTree,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> std::result::Result<(), fmt::Error> {
        match self.storage.tree().root() {
            Some(root) => self.fmt_inner(f, root, 0),
            None => write!(f, "Empty tree"),
        }
    }
}

#[cfg(test)]
mod test {
    use proptest::{self as prop, prelude::*};

    use super::super::flatten::{FullLayout, TreeLayout};
    use super::super::test::*;
    use super::*;

    const BLOCK_SIZE: usize = 1024;

    fn make_tree(data: &[u8]) -> HashTree<Sha256Hasher, SizedTestTreeStorage> {
        FullLayout::make_builder(&SHA256_STANDARD, Vec::new())
            .consume_and_finish(data)
            .unwrap()
    }

    fn make_data(size: usize) -> Vec<u8> {
        let cap = ((size + 3) / 4) * 4;
        let mut data = vec![0; cap];

        for (i, c) in data.chunks_mut(4).enumerate() {
            c[0] = (i >> 0) as u8;
            c[1] = (i >> 8) as u8;
            c[2] = (i >> 16) as u8;
            c[3] = (i >> 24) as u8;
        }

        data.truncate(size);
        data
    }

    fn make_empty(
        data_size: usize,
        root: &<Sha256Hasher as Hasher>::Output,
    ) -> HashTree<Sha256Hasher, SizedTestTreeStorage> {
        let s_r = FullLayout::from(SHA256_STANDARD).storage_required(data_size as u64);
        let storage = vec![0; s_r as usize].into_boxed_slice();
        FullLayout::prepare_empty(&SHA256_STANDARD, storage, root).unwrap()
    }

    #[test]
    fn root_hash() {
        fn root(data: &[u8]) -> NodeContent<TigerOut> {
            let res = make_tree(data).root_hash().unwrap();
            res
        }

        assert_eq!(
            root(&vec![0u8; 0]),
            NodeContent::filled(TigerOut::from_slice(&hex!(
                "6e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d"
            )))
        );
        assert_eq!(
            root(&vec![0u8; 1]),
            NodeContent::filled(TigerOut::from_slice(&hex!(
                "96a296d224f285c67bee93c30f8a309157f0daa35dc5b87e410b78630a09cfc7"
            )))
        );
        assert_eq!(
            root(&vec![0x41u8; 1024]),
            NodeContent::filled(TigerOut::from_slice(&hex!(
                "a98290f20b0b1db9960733a53b0558bf48b8a1addcbda14d897f8ed5cbca942d"
            )))
        );
        assert_eq!(
            root(&vec![0x41u8; 1025]),
            NodeContent::filled(TigerOut::from_slice(&hex!(
                "65b059e210a3dd84717771dbe4f7a8c9db460ba5b0e3eebbc4c4f56cad6ac76f"
            )))
        );
        assert_eq!(
            root(&vec![0x41u8; 2048]),
            NodeContent::filled(TigerOut::from_slice(&hex!(
                "6b294bfff03dec8fd30556348fad174655594e1b9d6dddbd20b008d5a23929f7"
            )))
        );
        assert_eq!(
            root(&vec![0x41u8; 2049]),
            NodeContent::filled(TigerOut::from_slice(&hex!(
                "a7ff5cbff1d5f0ea20fe53fbd47738c3261a36a3546b6d0ea07aaf75898031a4"
            )))
        );
        assert_eq!(
            root(&vec![0x41u8; 3072]),
            NodeContent::filled(TigerOut::from_slice(&hex!(
                "3a4494716df46d6e56ef976ffc1e78c6d1694b6cd2078a3dea0ff9d4ebe16a6c"
            )))
        );
        assert_eq!(
            root(&vec![0x41u8; 3073]),
            NodeContent::filled(TigerOut::from_slice(&hex!(
                "87f17bc911756393c10fcf97755681e2f4419681832262d84730a6488be729f7"
            )))
        );
        assert_eq!(
            root(&vec![0x41u8; 6144]),
            NodeContent::filled(TigerOut::from_slice(&hex!(
                "c87b598a5cc6adddd406c9a70474f8b972cd7889e2dcc8a870626cd34725ddd8"
            )))
        );
        assert_eq!(
            root(&vec![0x41u8; 54321]),
            NodeContent::filled(TigerOut::from_slice(&hex!(
                "925df06155cbb6ef504125c425d2eed078f4eb00f8fe712dae54f70bc01fccb3"
            )))
        );
    }

    #[test]
    fn patch_for() {
        let data: Vec<u8> = (0..256)
            .flat_map(|i| vec![i as u8; BLOCK_SIZE].into_iter())
            .collect();
        let ht = make_tree(&data);

        for i in 0..256 {
            let res = ht
                .patch_for(Leaf(i)..Leaf(i + 1), Leaf(0)..Leaf(256))
                .unwrap();
            assert_eq!(res.data.get(i).unwrap()[0], i as u8);
        }

        assert!(
            ht.patch_for(Leaf(1024)..Leaf(1025), Leaf(0)..Leaf(256))
                .is_err()
        );
    }

    #[test]
    fn patch_required() {
        let blocks = 63;
        let orig = make_tree(&make_data(blocks * BLOCK_SIZE));
        let empty = make_empty(blocks * BLOCK_SIZE, &orig.root_hash().unwrap().hash);

        assert_eq!(orig.tree.last_leaf(), empty.tree.last_leaf());

        for i in 0..blocks {
            assert_eq!(
                empty.read_data_blocks(Leaf(i)..Leaf(i + 1)),
                Err(TreeError::Missing(
                    Leaf(i)..Leaf(i + 1),
                    Leaf(0)..empty.tree.last_leaf().unwrap().next()
                ))
            );
        }
    }

    fn blocks_and_splits(
        range: std::ops::Range<usize>,
    ) -> impl Strategy<Value = (usize, Vec<usize>)> {
        range.prop_flat_map(|size| {
            let vec: Vec<_> = (1..size).collect();
            let splits_strategy = match vec.len() {
                0 => Just(vec).boxed(),
                vec_len => prop::sample::subsequence(vec, 0..vec_len).boxed(),
            };
            (
                Just(size),
                splits_strategy
                    .prop_map(move |mut vec| {
                        vec.extend_from_slice(&[0, size]);
                        vec
                    })
                    .prop_shuffle(),
            )
        })
    }

    proptest! {
        #[test]
        fn full_replication((blocks, splits) in blocks_and_splits(1..33)) {
            let a = Some(0).into_iter().chain(splits.clone().into_iter());
            let b = splits.into_iter().chain(Some(blocks));
            let data = make_data(blocks * BLOCK_SIZE);
            let orig = make_tree(&data);
            let mut target = make_empty(blocks * BLOCK_SIZE, &orig.root_hash().unwrap().hash);

            for (a, b) in a.zip(b) {
                let range = if a < b {Leaf(a)..Leaf(b)} else {Leaf(b)..Leaf(a)};
                loop {
                    match target.read_data_blocks(range.clone()) {
                        Ok(block) => {
                            assert_eq!(block, data[range.start.0*BLOCK_SIZE..range.end.0*BLOCK_SIZE]);
                            break;
                        },
                        Err(TreeError::Missing(data_range, proof_range)) => {
                            let patch = orig.patch_for(data_range, proof_range).expect("Patch read OK");
                            target.merge(patch).expect("Patch merged OK");
                        }
                        Err(e) => panic!("Unexpected Error {:?}", e),
                    }
                }
            }
            assert!(target.root_hash().expect("Readable").is_filled);
        }
    }
}
