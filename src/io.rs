use super::index::{Leaf, LeafRange};
use super::model::Bytes;

use std;
use std::cmp::min;
use std::io;

use bytes::{BufMut, BytesMut};

pub struct BlockBuffer {
    block_size: usize,
    buffer: BytesMut,
}

impl BlockBuffer {
    pub fn new(block_size: usize) -> Self {
        Self {
            block_size,
            buffer: BytesMut::with_capacity(block_size),
        }
    }

    pub fn consume_slice<Tgt, E>(&mut self, s: &[u8], mut tgt: Tgt) -> Result<(), E>
    where
        Tgt: FnMut(&[u8]) -> Result<(), E>,
    {
        let mut s = match self.buffer.len() {
            0 => s,
            l => {
                let needed = self.block_size - l;
                if needed > s.len() {
                    self.buffer.put_slice(s);
                    &s[l..l]
                } else {
                    let (head, remainder) = s.split_at(needed);
                    self.buffer.put_slice(head);
                    tgt(&self.buffer)?;
                    self.buffer.clear();
                    remainder
                }
            }
        };

        while s.len() >= self.block_size {
            let (block, remainder) = s.split_at(self.block_size);
            tgt(block)?;
            s = remainder;
        }

        if !s.is_empty() {
            self.buffer.put_slice(s);
        }
        Ok(())
    }

    pub fn consume_reader<R, Tgt, E>(&mut self, mut r: R, mut tgt: Tgt) -> Result<(), E>
    where
        R: io::Read,
        Tgt: FnMut(&[u8]) -> Result<(), E>,
        E: From<io::Error>,
    {
        while self.fill_buffer(&mut r)? > 0 {
            while self.buffer.len() > self.block_size {
                tgt(&self.buffer[..self.block_size])?;
                self.buffer.advance(self.block_size);
            }
        }
        Ok(())
    }

    fn fill_buffer<R: io::Read>(&mut self, r: &mut R) -> io::Result<usize> {
        self.buffer.reserve(4 * self.block_size);
        unsafe {
            let read = r.read(self.buffer.bytes_mut())?;
            self.buffer.advance_mut(read);
            Ok(read)
        }
    }

    pub fn flush<Tgt, E>(&mut self, mut tgt: Tgt) -> Result<(), E>
    where
        Tgt: FnMut(&[u8]) -> Result<(), E>,
    {
        if !self.buffer.is_empty() {
            tgt(&self.buffer)?;
            self.buffer.clear();
        }
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct Blocks {
    blocksize: usize,
    data: Bytes,
}

impl Blocks {
    pub fn new(blocksize: usize, data: Bytes) -> Self {
        Self { blocksize, data }
    }

    pub fn count(&self) -> usize {
        (self.data.len() + self.blocksize - 1) / self.blocksize
    }

    pub fn with_offset(self, o: usize) -> BlockSlice {
        BlockSlice {
            startblock: o,
            blocks: self,
        }
    }

    pub fn get(&self, idx: usize) -> Option<&[u8]> {
        let offset = idx * self.blocksize;
        if offset < self.data.len() {
            let stop = min(offset + self.blocksize, self.data.len());
            Some(&self.data[offset..stop])
        } else {
            None
        }
    }

    pub fn iter(&self) -> std::slice::Chunks<u8> {
        self.data.chunks(self.blocksize)
    }
}

#[derive(Debug, Clone)]
pub struct BlockSlice {
    startblock: usize,
    blocks: Blocks,
}

impl BlockSlice {
    pub fn count(&self) -> usize {
        self.blocks.count()
    }

    pub fn leaves(&self) -> LeafRange {
        Leaf(self.startblock)..Leaf(self.startblock + self.count())
    }

    pub fn get(&self, idx: usize) -> Option<&[u8]> {
        idx.checked_sub(self.startblock)
            .and_then(|i| self.blocks.get(i))
    }

    pub fn iter<'a, 'b: 'a>(&'b self) -> impl 'a + Iterator<Item = (usize, &[u8])> {
        self.blocks
            .iter()
            .enumerate()
            .map(move |(i, b)| (self.startblock + i, b))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use std::io;

    struct BlockVerification {
        block_size: usize,
        whole_blocks: usize,
        last_block: usize,
    }

    impl BlockVerification {
        fn new(block_size: usize) -> Self {
            Self {
                block_size,
                whole_blocks: 0,
                last_block: 0,
            }
        }

        fn push(&mut self, block: &[u8]) -> io::Result<()> {
            assert_eq!(self.last_block, 0);
            assert!(!block.is_empty());
            assert!(block.len() <= self.block_size);
            if block.len() == self.block_size {
                self.whole_blocks += 1;
            } else {
                self.last_block = block.len();
            }
            Ok(())
        }

        fn len(&self) -> usize {
            (self.block_size * self.whole_blocks) + self.last_block
        }
    }

    proptest! {
        #[test]
        fn test_block_buffer_reader(input_size in 1..512usize) {
            let block_size = 13;
            let mut block_buffer = BlockBuffer::new(block_size);
            let mut verification = BlockVerification::new(block_size);
            let data = vec![0; input_size];
            block_buffer.consume_reader(data.as_slice(), |block| verification.push(block)).expect("Consumed");
            block_buffer.flush(|block| verification.push(block)).expect("Flushed");

            assert_eq!(verification.len(), input_size);
        }
    }

    proptest! {
        #[test]
        fn test_block_buffer_slice(input_size in 1..512usize) {
            let block_size = 13;
            let mut block_buffer = BlockBuffer::new(block_size);
            let mut verification = BlockVerification::new(block_size);
            let data = vec![0; input_size];
            block_buffer.consume_slice(data.as_slice(), |block| verification.push(block)).expect("Consumed");
            block_buffer.flush(|block| verification.push(block)).expect("Flushed");

            assert_eq!(verification.len(), input_size);
        }
    }
}
