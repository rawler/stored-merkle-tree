//! Index-types for adressing nodes in a tree

use std::cmp::Ordering;
use std::ops::Range;

pub type Level = u8;

/// Operations on indicies
pub trait Index: PartialOrd + Sized {
    fn leaves_before(&self) -> usize;
    fn level(&self) -> Level;
    fn parent(&self) -> Branch;
    fn root(&self) -> Node;
    fn sibling(&self) -> Self;
    fn to_node(self) -> Node;

    fn siblings(self) -> (Self, Self) {
        let sibling = self.sibling();
        if self < sibling {
            (self, sibling)
        } else {
            (sibling, self)
        }
    }
}

/// Inner node of tree. Always has at least a left child, often also a right
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, PartialOrd, Ord)]
pub struct Branch(pub usize);

/// Edge node of tree. Holds the hash of a single block
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, PartialOrd, Ord)]
pub struct Leaf(pub usize);

/// Either a Branch or Leaf node
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum Node {
    Branch(usize),
    Leaf(usize),
}

impl Branch {
    fn msb(x: usize) -> u8 {
        use std::mem::size_of_val;

        let bits_in_val = size_of_val(&x) as u8 * 8;
        bits_in_val - x.leading_zeros() as u8
    }

    fn n_1s(n: u8) -> usize {
        (1 << n) - 1
    }

    fn root_level(&self) -> u8 {
        Self::msb(self.0 + 1) - 1
    }

    fn _level(&self) -> u8 {
        (!self.0).trailing_zeros() as u8
    }

    pub fn right_child(&self) -> Node {
        match self._level() {
            0 => Node::leaf(self.0 | 0b1),
            level => {
                let mask = 0b1 << level;
                Node::branch((self.0 | mask) & !(mask >> 1))
            }
        }
    }
    pub fn left_child(&self) -> Node {
        match self._level() {
            0 => Node::leaf(self.0 & !0b1),
            level => {
                let mask = 0b1 << (level - 1);
                Node::branch(self.0 & !mask)
            }
        }
    }
}

impl PartialEq<Leaf> for Branch {
    fn eq(&self, _: &Leaf) -> bool {
        false
    }
}

impl PartialOrd<Leaf> for Branch {
    fn partial_cmp(&self, leaf: &Leaf) -> Option<Ordering> {
        if self.0 < leaf.0 {
            Some(Ordering::Less)
        } else {
            Some(Ordering::Greater)
        }
    }
}

impl Index for Branch {
    fn leaves_before(&self) -> usize {
        self.0 + 1
    }
    fn level(&self) -> Level {
        1 + self._level()
    }
    fn parent(&self) -> Self {
        let mask = 0b1 << self._level();
        Branch((self.0 | mask) & !(mask << 1))
    }
    fn root(&self) -> Node {
        Node::branch(Self::n_1s(self.root_level()))
    }
    fn sibling(&self) -> Self {
        let mask = 0b10 << self._level();
        Branch(self.0 ^ mask)
    }
    fn to_node(self) -> Node {
        Node::Branch(self.0)
    }
}

impl Leaf {
    pub fn new(idx: usize) -> Self {
        Leaf(idx)
    }

    pub fn index(&self) -> usize {
        self.0
    }

    pub fn next(&self) -> Leaf {
        Leaf(self.0 + 1)
    }

    pub fn range(r: Range<usize>) -> LeafRange {
        Leaf(r.start)..Leaf(r.end)
    }
}

impl PartialEq<Branch> for Leaf {
    fn eq(&self, _: &Branch) -> bool {
        false
    }
}

impl PartialOrd<Branch> for Leaf {
    fn partial_cmp(&self, tree: &Branch) -> Option<Ordering> {
        tree.partial_cmp(self)
    }
}

impl Index for Leaf {
    fn leaves_before(&self) -> usize {
        self.0
    }
    fn level(&self) -> Level {
        0
    }
    fn sibling(&self) -> Self {
        Leaf(self.0 ^ 0b1)
    }
    fn parent(&self) -> Branch {
        Branch(self.0 & !0b1)
    }
    fn root(&self) -> Node {
        match self.0 {
            0 => Node::Leaf(0),
            _ => self.parent().root(),
        }
    }
    fn to_node(self) -> Node {
        Node::Leaf(self.0)
    }
}

impl Node {
    pub fn leaf(i: usize) -> Node {
        Node::Leaf(i)
    }
    pub fn branch(i: usize) -> Node {
        Node::Branch(i)
    }
    pub fn unwrap_tree(self) -> Branch {
        match self {
            Node::Branch(t) => Branch(t),
            Node::Leaf(_) => panic!("Node expected to be branch is really a leaf"),
        }
    }

    pub fn leftmost_descendant(self) -> Leaf {
        match self {
            Node::Branch(t) => Branch(t).left_child().leftmost_descendant(),
            Node::Leaf(l) => Leaf(l),
        }
    }

    pub fn rightmost_descendant(self) -> Leaf {
        match self {
            Node::Branch(t) => Branch(t).right_child().rightmost_descendant(),
            Node::Leaf(l) => Leaf(l),
        }
    }
}

impl PartialOrd<Node> for Node {
    fn partial_cmp(&self, other: &Node) -> Option<Ordering> {
        match (self, other) {
            (&Node::Leaf(a), &Node::Leaf(b)) => Some(a.cmp(&b)),
            (&Node::Branch(a), &Node::Branch(b)) => Some(a.cmp(&b)),
            (&Node::Leaf(a), &Node::Branch(b)) => Some(if a <= b {
                Ordering::Less
            } else {
                Ordering::Greater
            }),
            (&Node::Branch(a), &Node::Leaf(b)) => Some(if a < b {
                Ordering::Less
            } else {
                Ordering::Greater
            }),
        }
    }
}

impl Ord for Node {
    fn cmp(&self, other: &Node) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl Index for Node {
    fn leaves_before(&self) -> usize {
        match *self {
            Node::Leaf(i) => Leaf(i).leaves_before(),
            Node::Branch(i) => Branch(i).leaves_before(),
        }
    }
    fn level(&self) -> Level {
        match *self {
            Node::Leaf(i) => Leaf(i).level(),
            Node::Branch(i) => Branch(i).level(),
        }
    }
    fn sibling(&self) -> Self {
        match *self {
            Node::Leaf(i) => Leaf(i).sibling().to_node(),
            Node::Branch(i) => Branch(i).sibling().to_node(),
        }
    }
    fn parent(&self) -> Branch {
        match *self {
            Node::Leaf(i) => Leaf(i).parent(),
            Node::Branch(i) => Branch(i).parent(),
        }
    }
    fn root(&self) -> Node {
        match *self {
            Node::Leaf(i) => Leaf(i).root(),
            Node::Branch(i) => Branch(i).root(),
        }
    }
    fn to_node(self) -> Node {
        self
    }
}

pub type LeafRange = Range<Leaf>;

/// Collections of nodes
pub trait Nodes {
    fn proofs(&self, horizon: LeafRange) -> Vec<Node> {
        let mut roots = self.roots().into_iter();
        let first_root = roots.next().unwrap_or_else(|| horizon.end.to_node());
        let last_root = roots.last().unwrap_or(first_root);

        let mut proof = (horizon.start..first_root.leftmost_descendant()).roots();
        proof.append(&mut (last_root.rightmost_descendant().next()..horizon.end).roots());

        proof
    }
    fn roots(&self) -> Vec<Node>;
}

impl Nodes for LeafRange {
    fn roots(&self) -> Vec<Node> {
        fn compact(res: &mut Vec<Node>) {
            while res.len() > 1 && res[res.len() - 1].sibling() == res[res.len() - 2] {
                let l = res.len();
                let parent = res[l - 1].parent();
                res.truncate(l - 2);
                res.push(parent.to_node());
            }
        }

        let mut res = Vec::new();
        let mut i = self.clone();
        while i.start < i.end {
            res.push(i.start.to_node());
            compact(&mut res);
            i.start = i.start.next();
        }

        res
    }
}

// Conditionally compile the module `test` only when the test-suite is run.
#[cfg(test)]
mod test {
    use super::*;
    use super::{Branch as T, Leaf as L};

    use proptest::sample::select;

    fn t(i: usize) -> Node {
        Node::branch(i)
    }

    fn l(i: usize) -> Node {
        Node::leaf(i)
    }

    #[test]
    fn parent() {
        assert_eq!(L(0).parent(), T(0));
        assert_eq!(T(0).parent(), T(1));
        assert_eq!(L(1).parent(), T(0));
        assert_eq!(T(1).parent(), T(3));
        assert_eq!(T(2).parent(), T(1));
        assert_eq!(T(4).parent(), T(5));
        assert_eq!(L(5).parent(), T(4));
        assert_eq!(L(6).parent(), T(6));
        assert_eq!(T(7).parent(), T(15));
    }

    #[test]
    fn root() {
        assert_eq!(L(0).root(), l(0));
        assert_eq!(T(0).root(), t(0));
        assert_eq!(L(1).root(), t(0));
        assert_eq!(T(1).root(), t(1));
        assert_eq!(T(2).root(), t(1));
        assert_eq!(L(3).root(), t(1));
        assert_eq!(T(3).root(), t(3));
        assert_eq!(L(7).root(), t(3));
        assert_eq!(T(7).root(), t(7));
    }

    #[test]
    fn sibling() {
        assert_eq!(L(0).sibling(), L(1));
        assert_eq!(T(0).sibling(), T(2));
        assert_eq!(L(1).sibling(), L(0));
        assert_eq!(T(1).sibling(), T(5));
        assert_eq!(T(2).sibling(), T(0));
        assert_eq!(L(3).sibling(), L(2));
        assert_eq!(T(3).sibling(), T(11));
        assert_eq!(T(4).sibling(), T(6));
        assert_eq!(L(6).sibling(), L(7));
        assert_eq!(L(7).sibling(), L(6));
        assert_eq!(T(7).sibling(), T(23));
    }

    #[test]
    fn leaves_before() {
        assert_eq!(L(0).leaves_before(), 0);
        assert_eq!(T(0).leaves_before(), 1);
        assert_eq!(L(1).leaves_before(), 1);
        assert_eq!(L(7).leaves_before(), 7);
        assert_eq!(T(7).leaves_before(), 8);
    }

    #[test]
    fn right_child() {
        assert_eq!(T(0).right_child(), l(1));
        assert_eq!(T(3).right_child(), t(5));
        assert_eq!(T(4).right_child(), l(5));
    }

    #[test]
    fn left_child() {
        assert_eq!(T(0).left_child(), l(0));
        assert_eq!(T(3).left_child(), t(1));
        assert_eq!(T(4).left_child(), l(4));
    }

    #[test]
    fn roots() {
        assert_eq!(Leaf::range(0..4).roots(), vec![t(1)]);
        assert_eq!(Leaf::range(4..11).roots(), vec![t(5), t(8), l(10)]);
    }

    #[test]
    fn proofs() {
        assert_eq!(
            Leaf::range(4..13).proofs(Leaf::range(0..16)),
            vec![t(1), l(13), t(14)]
        );
        assert_eq!(
            Leaf::range(4..8).proofs(Leaf::range(0..16)),
            vec![t(1), t(11)]
        );
        assert_eq!(
            Leaf::range(4..12).proofs(Leaf::range(0..13)),
            vec![t(1), l(12)]
        );
        assert_eq!(
            Leaf::range(0..1).proofs(Leaf::range(0..13)),
            vec![l(1), t(2), t(5), t(9), l(12)]
        );
    }

    fn ordered_nodes() -> Vec<Node> {
        vec![
            Node::leaf(0),
            Node::branch(0),
            Node::leaf(1),
            Node::branch(1),
            Node::leaf(2),
            Node::branch(2),
            Node::leaf(3),
            Node::branch(3),
            Node::leaf(4),
            Node::branch(4),
        ]
    }

    proptest! {
        #[test]
        fn partial_ord(ref a in select(ordered_nodes()), ref b in select(ordered_nodes())) {
            let a_i = ordered_nodes().iter().position(|&r| r == *a).unwrap();
            let b_i = ordered_nodes().iter().position(|&r| r == *b).unwrap();
            assert_eq!((*a).partial_cmp(b), a_i.partial_cmp(&b_i));
        }
    }
}
