//! Merkle Tree Hashing implementations

use super::index::Node;
use super::io::Blocks;
use super::model::{ByteArray, Bytes, NodeContent};

use std::borrow::Borrow;
use std::fmt::LowerHex;
use std::marker::PhantomData;

use digest::generic_array::{ArrayLength, GenericArray};
use digest::{Digest, FixedOutput};

use sha2::Sha256;

/// An implementation for a TreeHasher
pub trait Hasher: Clone {
    type Output: ByteArray;

    fn block_size(&self) -> usize;
    fn leaf(&self, buf: &[u8]) -> Self::Output;
    fn tree(&self, left: &[u8], right: &[u8]) -> Self::Output;

    fn leaf_node(&self, buf: &[u8]) -> NodeContent<Self::Output> {
        NodeContent {
            is_filled: true,
            hash: self.leaf(buf),
        }
    }
    fn merge_tree(
        &self,
        l: &NodeContent<Self::Output>,
        r: &NodeContent<Self::Output>,
    ) -> NodeContent<Self::Output> {
        NodeContent {
            is_filled: l.is_filled && r.is_filled,
            hash: self.tree(l.hash.borrow(), r.hash.borrow()),
        }
    }

    fn merge_tree_unsorted(
        &self,
        a: (&Node, &NodeContent<Self::Output>),
        b: (&Node, &NodeContent<Self::Output>),
    ) -> NodeContent<Self::Output> {
        if a.0 < b.0 {
            self.merge_tree(a.1, b.1)
        } else {
            self.merge_tree(b.1, a.1)
        }
    }

    fn split(&self, data: Bytes) -> Blocks {
        Blocks::new(self.block_size(), data)
    }
}

/// Adapter for mapping a `digest`-crate implementation into a THEX BoundTree Hasher
#[derive(Clone, Debug)]
pub struct DigestHasher<D>
where
    D: Clone + Digest,
{
    block_size: usize,
    _phantom: PhantomData<D>,
}

/// Convenience type for hashing using Sha256
pub type Sha256Hasher = DigestHasher<Sha256>;

/// Convenience-instance of a Sha256 hasher
pub const SHA256_STANDARD: Sha256Hasher = DigestHasher {
    block_size: 1024,
    _phantom: PhantomData,
};

impl<L: ArrayLength<u8>> ByteArray for GenericArray<u8, L>
where
    L::ArrayType: Copy,
    GenericArray<u8, L>: LowerHex,
{
    fn len() -> usize {
        L::to_usize()
    }

    fn from_slice(s: &[u8]) -> Self {
        *Self::from_slice(s)
    }
}

impl<D> Hasher for DigestHasher<D>
where
    D: Clone + Digest,
    <<D as FixedOutput>::OutputSize as ArrayLength<u8>>::ArrayType: Copy,
    GenericArray<u8, <D as FixedOutput>::OutputSize>: ByteArray,
{
    type Output = GenericArray<u8, <D as FixedOutput>::OutputSize>;

    fn block_size(&self) -> usize {
        self.block_size
    }

    fn leaf(&self, buf: &[u8]) -> Self::Output {
        assert!(buf.len() <= self.block_size);
        let mut d = D::new();
        d.input(&[0u8]);
        d.input(buf);
        d.result()
    }
    fn tree(&self, left: &[u8], right: &[u8]) -> Self::Output {
        let mut d = D::new();
        d.input(&[1u8]);
        d.input(left);
        d.input(right);
        d.result()
    }
}

/// Folds a given amount of levels from another hasher. The effect is bigger
/// block-size, reducing overhead of intermediate hashes, while preserving the
/// root hash
#[derive(Clone, Debug)]
pub struct Fold<Folded: Hasher> {
    folded: Folded,
    levels: u8,
}

impl<Folded> Hasher for Fold<Folded>
where
    Folded: Hasher,
{
    type Output = Folded::Output;

    fn block_size(&self) -> usize {
        self.folded.block_size() << self.levels
    }

    fn leaf(&self, data: &[u8]) -> Folded::Output {
        let folded_block_size = self.folded.block_size();
        if data.len() <= folded_block_size {
            return self.folded.leaf(data);
        }

        let mut buf: Vec<Option<Folded::Output>> = vec![None; self.levels as usize + 1];

        for block in data.chunks(folded_block_size) {
            let mut hash = self.folded.leaf(block);
            for buf_place in &mut buf {
                match *buf_place {
                    Some(first_hash) => {
                        hash = self.folded.tree(first_hash.borrow(), hash.borrow());
                        *buf_place = None;
                    }
                    None => {
                        *buf_place = Some(hash);
                        break;
                    }
                }
            }
        }

        let mut iter_buf = buf.into_iter().filter_map(|x| x);
        let mut res = iter_buf.next().unwrap();
        for hash in iter_buf {
            res = self.folded.tree(hash.borrow(), res.borrow())
        }
        res
    }
    fn tree(&self, left: &[u8], right: &[u8]) -> Folded::Output {
        self.folded.tree(left, right)
    }
}

#[cfg(test)]
mod test {
    use super::super::*;
    use super::*;

    fn test_fold_hasher_(block_count: usize, levels: u8) {
        let data = vec![0u8; block_count];
        let folder = Fold {
            folded: SHA256_STANDARD,
            levels: levels,
        };

        let tree1 = flatten::FullLayout::make_builder(&SHA256_STANDARD, Vec::new())
            .consume_and_finish(data.as_slice())
            .expect("Created OK");
        let tree2 = flatten::FullLayout::make_builder(&folder, Vec::new())
            .consume_and_finish(data.as_slice())
            .expect("Created OK");

        assert!(tree1.root_hash().is_ok());
        assert_eq!(tree1.root_hash().unwrap(), tree2.root_hash().unwrap());
    }

    proptest! {
        #[test]
        fn test_fold_hasher(block_count in 0..1024usize, levels in 0..11u8) {
            test_fold_hasher_(block_count, levels)
        }
    }
}
