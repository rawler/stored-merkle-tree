//! Storage model for reading and writing tree-nodes from/to persistent storage
//!
//! Intended for custom-implementation of storage models, such as to database or
//! custom storage-formats.
//!
//! See Also: [flatten](../flatten/index.html)

//!
//! ```
//! # #[macro_use]
//! # extern crate hex_literal;
//! # extern crate failure;
//! # extern crate stored_merkle_tree;
//! #
//! # use failure::Error;
//! # use stored_merkle_tree::*;
//! # use stored_merkle_tree::error::*;
//! # use stored_merkle_tree::hasher::*;
//! # use stored_merkle_tree::index::*;
//! # use stored_merkle_tree::model::*;
//! #
//!
//! struct Asset<'asset, H: Hasher> {
//!     hashes: flatten::TreeStorage<flatten::HashOnlyLayout<H>, Box<[u8]>>,
//!     data: &'asset [u8],
//!     block_size: usize,
//! }
//!
//! impl<'asset, H: Hasher> HashReader for Asset<'asset, H> {
//!     type HashValue = H::Output;
//!
//!     fn read_hash(
//!         &self,
//!         idx: &Node,
//!     ) -> Result<Option<NodeContent<Self::HashValue>>, std::io::Error> {
//!         self.hashes.read_hash(idx)
//!     }
//! }
//!
//! impl<'asset, H: Hasher> DataReader for Asset<'asset, H> {
//!     fn read_data(&self, idx: &Leaf) -> Result<Option<Bytes>, std::io::Error> {
//!         let offset = idx.index() * self.block_size;
//!         let result = &self.data[offset..offset + self.block_size];
//!         Ok(Some(result.into()))
//!     }
//! }
//!
//! impl<'asset, H: Hasher> SizedTree for Asset<'asset, H> {
//!     fn tree(&self) -> BoundTree {
//!         self.hashes.tree()
//!     }
//! }
//!
//! impl<'asset, H: Hasher> Asset<'asset, H> {
//!     fn new_tree(hasher: &H, data: &'asset [u8]) -> Result<HashTree<H, Self>, CreationError> {
//!         let block_size = hasher.block_size();
//!         let hashes = HashOnlyLayout::make_builder(hasher, Vec::new())
//!             .consume_and_finish(data)?
//!             .unwrap_storage();
//!         let res = Self {
//!             hashes,
//!             data,
//!             block_size,
//!         };
//!         HashTree::new(hasher, res)
//!     }
//! }
//!
//! fn try_main() -> Result<(), Error> {
//!     let data = [0u8; 1024 * 1024 * 4];
//!     let asset = Asset::new_tree(&SHA256_STANDARD, &data)?;
//!
//!     // Hashes content properly
//!     let root = asset.root_hash()?;
//!     assert!(root.is_filled);
//!
//!     // Merges hashes and data
//!     let patch = asset.patch_for(Leaf(0)..Leaf(1), Leaf(0)..Leaf(64))?;
//!     assert!(patch.data_blocks().count() > 0);
//!
//!     Ok(())
//! }
//!
//! #
//! # fn main() { try_main().unwrap(); }
//! ```

use super::error::{CreationError, TreeError};
use super::index::{Index, Leaf, Node};
use super::{to_hex_string, TreeDumper};

use std;
use std::borrow::Borrow;
use std::fmt::{self, Debug};

pub use super::tree::BoundTree;
pub use bytes::Bytes;

pub trait ByteArray:
    Borrow<[u8]> + Clone + Copy + Debug + PartialEq<Self> + PartialOrd<Self> + fmt::LowerHex
{
    fn len() -> usize;

    fn from_slice(&[u8]) -> Self;
}

/// The contents of every node of a tree, it's hash, and whether all leafs under
/// this nodes is filled or not
#[derive(Copy, Clone, Debug, Hash, PartialEq, PartialOrd)]
pub struct NodeContent<A: ByteArray> {
    pub is_filled: bool,
    pub hash: A,
}

impl<A: ByteArray> NodeContent<A> {
    pub fn filled(hash: &A) -> Self {
        Self {
            is_filled: true,
            hash: *hash,
        }
    }

    pub fn rumored(hash: &A) -> Self {
        Self {
            is_filled: false,
            hash: *hash,
        }
    }

    pub(crate) fn is_update_over<Content: Borrow<NodeContent<A>>>(
        &self,
        old: Option<Content>,
    ) -> Result<bool, (A, A)> {
        match old {
            None => Ok(true),
            Some(hn) => {
                let hn = hn.borrow();
                if hn.hash == self.hash {
                    Ok(self.is_filled > hn.is_filled)
                } else {
                    Err((hn.hash, self.hash))
                }
            }
        }
    }
}

impl<A: ByteArray> fmt::LowerHex for NodeContent<A> {
    fn fmt(&self, f: &mut fmt::Formatter) -> std::result::Result<(), fmt::Error> {
        if self.is_filled {
            write!(f, "Confirmed({})", to_hex_string(self.hash))
        } else {
            write!(f, "Rumored({})", to_hex_string(self.hash))
        }
    }
}

/// Read hash-data from persistent storage
pub trait HashReader {
    type HashValue: ByteArray;

    fn read_hash(&self, idx: &Node)
        -> Result<Option<NodeContent<Self::HashValue>>, std::io::Error>;
}

/// Write nodes to persistent storage
pub trait NodeWriter: HashReader {
    fn write_node(
        &mut self,
        idx: &Node,
        hash: &NodeContent<Self::HashValue>,
    ) -> Result<(), std::io::Error>;

    fn write_data(&mut self, idx: &Leaf, data: &[u8]) -> Result<(), std::io::Error>;

    fn write_leaf(
        &mut self,
        idx: &Leaf,
        hv: &Self::HashValue,
        data: &[u8],
    ) -> Result<(), std::io::Error> {
        self.write_data(idx, data)?;
        self.write_node(&idx.to_node(), &NodeContent::filled(hv))
    }

    fn change(
        &mut self,
        idx: &Node,
        new: &NodeContent<Self::HashValue>,
    ) -> Result<bool, TreeError> {
        let existing = self.read_hash(idx)?;
        match new.is_update_over(existing) {
            Ok(false) => Ok(false),
            Ok(true) => {
                self.write_node(idx, &new)?;
                Ok(true)
            }
            Err((old, new)) => Err(TreeError::HashConflict(
                *idx,
                old.borrow().into(),
                new.borrow().into(),
            )),
        }
    }
}

/// Writer capable of writing new nodes past the currently last node
pub trait ExtendableNodeWriter: NodeWriter {
    type Sized: NodeWriter<HashValue = Self::HashValue> + SizedTree;

    fn try_into_sized(self) -> Result<Self::Sized, CreationError>;
}

/// Read data-blocks from persistent storage
pub trait DataReader {
    fn read_data(&self, idx: &Leaf) -> Result<Option<Bytes>, std::io::Error>;
}

/// Tree of fixed size
pub trait SizedTree {
    fn tree(&self) -> BoundTree;
}

/// Convenience-combination of a complete tree-implementation
pub trait TreeStorage: HashReader + DataReader + SizedTree {
    fn dump(&self) -> TreeDumper<Self>
    where
        Self: std::marker::Sized,
    {
        TreeDumper::new(self, false)
    }
}
