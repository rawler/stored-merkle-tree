//! This crate provides components for encoding and incrementally transferring
//! [Merkle Trees][mtrees].
//!
//! This crate differs from other merkle-tree crates (at time of writing) by its
//! focus on trees that may not necessarily be stored in memory, but held in some
//! persistent storage, such as a file or database.
//!
//! Instead, the crate has a more narrow focus on Merkle Trees as means of
//! checksumming files of uniformly-sized blocks. The primary target is the
//! [Tree Hash Exchange Format][thex] but the design is held open for client
//! -code to implement custom ways of storing hashes and data, as well as exact
//! protocol for transferring patches.
//!
//! [mtrees]: https://en.wikipedia.org/wiki/Merkle_tree
//! [thex]: http://web.archive.org/web/20081227123433/http://open-content.net/specs/draft-jchapweske-thex-02.html
//!
//! ```
//! # #[macro_use]
//! # extern crate hex_literal;
//! # extern crate failure;
//! # extern crate stored_merkle_tree;
//! #
//! # use failure::Error;
//! # use stored_merkle_tree::*;
//! # use std::{env, fs};
//! #
//! # fn try_main() -> Result<(), Error> {
//! #
//! let path = env::temp_dir().join("stored.tree");
//! let storage = fs::OpenOptions::new()
//!     .read(true)
//!     .write(true)
//!     .create(true)
//!     .truncate(true)
//!     .open(path)?;
//! let builder = FullLayout::make_builder(&SHA256_STANDARD, storage);
//! let tree = builder.consume_and_finish(&b"AAAA"[..])?;
//!
//! let root = tree.root_hash()?;
//! assert!(root.is_filled);
//!
//! assert_eq!(root.hash.as_ref(), hex!("4a134cec08c3aad0a7d47ef8ce0c5d23f3e3865e1c9c2483458fbb2268aada99"));
//! #
//! #     Ok(())
//! # }
//! #
//! # fn main() { try_main().unwrap(); }
//! ```

#[macro_use]
extern crate failure_derive;

extern crate block_buffer;
extern crate byte_tools;
extern crate bytes;
extern crate digest;
extern crate failure;
extern crate libc;
extern crate nix;
extern crate sha2;

#[cfg(test)]
#[macro_use]
extern crate hex_literal;

#[cfg(test)]
#[macro_use]
extern crate assert_matches;

#[cfg(test)]
#[macro_use]
extern crate proptest;

mod byte_storage;
mod hashtree;
mod io;
mod patch;
mod tree;

pub mod error;
pub mod flatten;
pub mod hasher;
pub mod index;
pub mod model;

pub use self::flatten::{FullLayout, HashOnlyLayout, SimpleSetup};
pub use self::hasher::SHA256_STANDARD;
pub use self::hashtree::{BufferedBuilder as Builder, HashTree, TreeDumper};
pub use self::patch::Patch;

use self::index::{Branch, Leaf, Node};
use self::model::{DataReader, HashReader, SizedTree, TreeStorage};

use std::borrow::Borrow;
use std::fmt::{self, Debug};

fn to_hex_string<T: Borrow<[u8]>>(bytes: T) -> String {
    let strs: Vec<String> = bytes
        .borrow()
        .iter()
        .map(|b| format!("{:02x}", b))
        .collect();
    strs.join("")
}

#[cfg(test)]
mod test {
    use super::model::ExtendableNodeWriter;
    use super::*;

    pub use super::hasher::{Hasher, SHA256_STANDARD, Sha256Hasher};
    pub use super::model::NodeWriter;

    pub type TestLayoutType = flatten::FullLayout<Sha256Hasher>;
    pub type TestTreeStorage = flatten::TreeStorage<TestLayoutType, Vec<u8>>;
    pub type SizedTestTreeStorage = <TestTreeStorage as ExtendableNodeWriter>::Sized;
    pub type TigerOut = <Sha256Hasher as Hasher>::Output;

    #[derive(Debug, Clone)]
    pub struct TreeBuilder {
        size: usize,
        root: Option<TigerOut>,
    }

    impl TreeBuilder {
        pub fn new(size: usize) -> TreeBuilder {
            TreeBuilder {
                size: size,
                root: None,
            }
        }

        pub fn with_root<H: Into<TigerOut>>(&self, hash: H) -> TreeBuilder {
            TreeBuilder {
                size: self.size,
                root: Some(hash.into()),
            }
        }

        pub fn build(&self) -> HashTree<Sha256Hasher, SizedTestTreeStorage> {
            use super::flatten::{FullLayout, TreeLayout};
            let layout = FullLayout::from(SHA256_STANDARD);
            let byte_storage =
                vec![0u8; layout.storage_required(self.size as u64) as usize].into_boxed_slice();
            let mut storage = SizedTestTreeStorage::new_bound(layout, byte_storage).unwrap();
            if let Some(root) = self.root {
                let root_idx = storage.tree().root().unwrap();
                storage
                    .write_node(&root_idx, &model::NodeContent::rumored(&root))
                    .unwrap()
            }
            HashTree::new(&SHA256_STANDARD, storage).expect("Tree Created")
        }
    }
}
