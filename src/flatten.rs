//! Logic for mapping a HashTree into flat-storage (File, Vec, ...)

use super::error::*;
use super::hasher::Hasher;
use super::index::{Index, Leaf, Node};
use super::model::*;
use super::tree::BoundTree;
use super::{Builder as _Builder, HashTree as _HashTree};

pub use super::byte_storage::{BoundByteStorage, ByteStorage, ExtendableByteStorage};

use std::io;

/// Maps nodes (hashes and optionally data-blocks) into offsets
pub trait TreeLayout: Clone {
    type DataLayout: DataLayout;
    type HashImpl: Hasher;

    fn leaves_for_size(&self, size: u64) -> Result<usize, CreationError>;
    fn data_layout(&self) -> Option<&Self::DataLayout>;
    fn node_size() -> usize;
    fn offset_of(&self, idx: &Node) -> u64;
    fn storage_required(&self, data_size: u64) -> u64;

    fn index_of(n: &Node) -> u64 {
        match *n {
            Node::Leaf(i) => (i as u64) << 1,
            Node::Branch(i) => ((i as u64) << 1) | 0b1,
        }
    }

    fn wrap<S: BoundByteStorage>(&self, s: S) -> Result<TreeStorage<Self, S>, CreationError> {
        TreeStorage::new_bound(self.clone(), s)
    }
    fn initialize_empty<S: ExtendableByteStorage>(&self, s: S) -> TreeStorage<Self, S> {
        TreeStorage::new_empty(self.clone(), s)
    }
}

/// Calculates offsets for data-blocks
pub trait DataLayout {
    fn block_size(&self) -> usize;
    fn data_offset(&self, idx: &Node) -> u64;
}

/// Standard layout for a flat-file containing only hashes
#[derive(Debug, Clone)]
pub struct HashOnlyLayout<H: Hasher>(H);

/// Standard layout for a flat-file containing data-blocks
#[derive(Debug, Clone)]
pub struct FullLayout<H: Hasher>(H);

impl<H: Hasher> From<H> for HashOnlyLayout<H> {
    fn from(h: H) -> Self {
        HashOnlyLayout(h)
    }
}

impl<H: Hasher> HashOnlyLayout<H> {}

impl<H: Hasher> From<H> for FullLayout<H> {
    fn from(h: H) -> Self {
        FullLayout(h)
    }
}

/// HashTree using given Layout and Storage
pub type HashTree<Layout, Storage> =
    _HashTree<<Layout as TreeLayout>::HashImpl, TreeStorage<Layout, Storage>>;
/// Builder using given Layout and Storage
pub type Builder<Layout, Storage> =
    _Builder<<Layout as TreeLayout>::HashImpl, TreeStorage<Layout, Storage>>;

/// Helper-trait, facilitating initialization from a given layout
pub trait SimpleSetup {
    type Layout: TreeLayout;

    fn prepare_empty<Storage>(
        h: &<Self::Layout as TreeLayout>::HashImpl,
        s: Storage,
        root: &<<Self::Layout as TreeLayout>::HashImpl as Hasher>::Output,
    ) -> Result<HashTree<Self::Layout, Storage>, CreationError>
    where
        Self::Layout: From<<Self::Layout as TreeLayout>::HashImpl>,
        Storage: BoundByteStorage,
    {
        Self::Layout::from(h.clone())
            .wrap(s)
            .and_then(|storage| HashTree::new_for(h, storage, root))
    }

    fn wrap_storage<Storage>(
        h: &<Self::Layout as TreeLayout>::HashImpl,
        s: Storage,
    ) -> Result<HashTree<Self::Layout, Storage>, CreationError>
    where
        Self::Layout: From<<Self::Layout as TreeLayout>::HashImpl>,
        Storage: BoundByteStorage,
    {
        Self::Layout::from(h.clone())
            .wrap(s)
            .and_then(|storage| HashTree::new(h, storage))
    }

    fn make_builder<Storage>(
        h: &<Self::Layout as TreeLayout>::HashImpl,
        s: Storage,
    ) -> Builder<Self::Layout, Storage>
    where
        Self::Layout: From<<Self::Layout as TreeLayout>::HashImpl>,
        Storage: ExtendableByteStorage,
    {
        let storage = Self::Layout::from(h.clone()).initialize_empty(s);
        Builder::new(h, storage)
    }
}

impl<Layout> SimpleSetup for Layout
where
    Layout: TreeLayout,
{
    type Layout = Self;
}

/// Dummy layout used as type of DataLayout when datalayout is missing
#[derive(Debug, Clone)]
pub struct NullDataLayout {}

impl DataLayout for NullDataLayout {
    fn block_size(&self) -> usize {
        unreachable!();
    }

    fn data_offset(&self, _p: &Node) -> u64 {
        unreachable!();
    }
}

impl<H: Hasher> TreeLayout for HashOnlyLayout<H> {
    type DataLayout = NullDataLayout;
    type HashImpl = H;

    fn node_size() -> usize {
        H::Output::len() + 1
    }

    fn offset_of(&self, idx: &Node) -> u64 {
        Self::index_of(idx) * (Self::node_size() as u64)
    }

    fn storage_required(&self, data_size: u64) -> u64 {
        let bs = self.0.block_size() as u64;
        let blocks = (data_size + bs - 1) / bs;
        self.offset_of(&Node::leaf(blocks as usize))
    }

    fn leaves_for_size(&self, size: u64) -> Result<usize, CreationError> {
        let ns = Self::node_size() as u64;
        if size % ns != 0 {
            return Err(CreationError::InvalidSize);
        }
        let nodes = (size / ns) as usize - 1;
        Ok((nodes + 1) / 2)
    }

    fn data_layout(&self) -> Option<&Self::DataLayout> {
        None
    }
}

impl<H: Hasher> TreeLayout for FullLayout<H> {
    type DataLayout = Self;
    type HashImpl = H;

    fn node_size() -> usize {
        H::Output::len() + 1
    }

    fn offset_of(&self, idx: &Node) -> u64 {
        let leaves_before = idx.leaves_before();
        (leaves_before as u64) * (self.0.block_size() as u64)
            + Self::index_of(idx) * (Self::node_size() as u64)
    }

    fn storage_required(&self, data_size: u64) -> u64 {
        if data_size == 0 {
            return 0;
        }
        let bs = self.0.block_size() as u64;
        let blocks = data_size / bs;
        let (last_index, overflow) = match data_size % bs {
            0 => (Node::leaf(blocks as usize - 1), bs),
            x => (Node::leaf(blocks as usize), x),
        };
        self.offset_of(&last_index) + overflow + Self::node_size() as u64
    }

    fn leaves_for_size(&self, size: u64) -> Result<usize, CreationError> {
        if size == 0 {
            return Ok(0);
        }
        let size = match size {
            0 => 0,
            size => size + Self::node_size() as u64,
        };

        let header_size = Self::node_size() * 2;
        let full_block_size = (self.0.block_size() + header_size) as u64;

        let full_blocks = (size / full_block_size) as usize;
        match (size % full_block_size) as usize {
            0 => Ok(full_blocks),
            x if x > header_size => Ok(full_blocks + 1),
            _ => Err(CreationError::InvalidSize),
        }
    }

    fn data_layout(&self) -> Option<&Self::DataLayout> {
        Some(self)
    }
}

impl<H: Hasher> DataLayout for FullLayout<H> {
    fn block_size(&self) -> usize {
        self.0.block_size()
    }

    fn data_offset(&self, idx: &Node) -> u64 {
        self.offset_of(idx) + Self::node_size() as u64
    }
}

/// Structure implementing storage of a Tree into a given flat byte-storage,
/// using a given Layout
#[derive(Debug)]
pub struct TreeStorage<L: TreeLayout, S: ByteStorage> {
    layout: L,
    storage: S,
    leaves: usize,
}

impl<L: TreeLayout, S: ByteStorage> TreeStorage<L, S> {
    pub fn new_bound(layout: L, storage: S) -> Result<TreeStorage<L, S>, CreationError>
    where
        S: BoundByteStorage,
    {
        let leaves = layout.leaves_for_size(storage.len())?;
        Ok(Self {
            layout,
            storage,
            leaves,
        })
    }

    pub fn new_empty(layout: L, storage: S) -> TreeStorage<L, S>
    where
        S: ExtendableByteStorage,
    {
        assert_eq!(storage.len(), 0);
        Self {
            layout,
            storage,
            leaves: 0,
        }
    }

    pub fn storage_size(&self) -> u64
    where
        S: BoundByteStorage,
    {
        self.storage.len()
    }

    fn extend_to(&mut self, block_index: usize) {
        if block_index >= self.leaves {
            self.leaves = block_index + 1;
        }
    }
}

impl<L: TreeLayout, S: ByteStorage> super::HashReader for TreeStorage<L, S> {
    type HashValue = <L::HashImpl as Hasher>::Output;

    fn read_hash(&self, idx: &Node) -> Result<Option<NodeContent<Self::HashValue>>, io::Error> {
        let node_start = self.layout.offset_of(idx);
        let from_storage = self.storage.read_all(node_start, L::node_size())?;
        let data = || Self::HashValue::from_slice(&from_storage[1..]);

        match from_storage[0] {
            0 => Ok(None),
            1 => Ok(Some(NodeContent::rumored(&data()))),
            2 => Ok(Some(NodeContent::filled(&data()))),
            x => Err(io::Error::new(
                io::ErrorKind::InvalidData,
                format!("Invalid data from Storage: Hashstate {:?} unknown", x),
            )),
        }
    }
}

impl<L: TreeLayout, S: ByteStorage> super::model::NodeWriter for TreeStorage<L, S> {
    fn write_node(
        &mut self,
        idx: &Node,
        hash: &NodeContent<Self::HashValue>,
    ) -> Result<(), io::Error> {
        use std::borrow::Borrow;
        let (tag, new_hash) = match *hash {
            NodeContent {
                is_filled: true,
                hash: h,
            } => (&[2u8], h),
            NodeContent {
                is_filled: false,
                hash: h,
            } => (&[1u8], h),
        };

        let offset = self.layout.offset_of(idx);

        self.storage.write(offset, tag)?;
        self.storage.write(offset + 1, new_hash.borrow())?;
        Ok(())
    }

    fn write_data(&mut self, idx: &Leaf, data: &[u8]) -> Result<(), io::Error> {
        if let Some(data_offset) = self
            .layout
            .data_layout()
            .map(|l| l.data_offset(&idx.to_node()))
        {
            self.storage.write(data_offset, data).map(|()| {
                self.extend_to(idx.index());
            })?;
        }
        Ok(())
    }
}

impl<L: TreeLayout, S: ExtendableByteStorage> ExtendableNodeWriter for TreeStorage<L, S> {
    type Sized = TreeStorage<L, S::BoundByteStorage>;

    fn try_into_sized(self) -> Result<Self::Sized, CreationError> {
        Self::Sized::new_bound(self.layout, self.storage.freeze_len())
    }
}

impl<L: TreeLayout, S: ByteStorage> super::DataReader for TreeStorage<L, S>
where
    L: DataLayout,
{
    fn read_data(&self, leaf: &Leaf) -> Result<Option<Bytes>, io::Error> {
        if let Some(NodeContent {
            is_filled: true, ..
        }) = self.read_hash(&leaf.to_node())?
        {
            let offset = self.layout.data_offset(&leaf.to_node());
            self.storage
                .read(offset, self.layout.block_size())
                .map(Some)
        } else {
            Ok(None)
        }
    }
}

impl<L: TreeLayout, S: BoundByteStorage> super::model::SizedTree for TreeStorage<L, S> {
    fn tree(&self) -> BoundTree {
        BoundTree {
            leaves: self.leaves,
        }
    }
}

impl<L: TreeLayout, S: BoundByteStorage> super::model::TreeStorage for TreeStorage<L, S> where
    L: DataLayout
{}

impl<L: TreeLayout, S: ByteStorage> Clone for TreeStorage<L, S>
where
    S: Clone,
{
    fn clone(&self) -> Self {
        Self {
            layout: self.layout.clone(),
            storage: self.storage.clone(),
            leaves: self.leaves,
        }
    }
}

#[cfg(test)]
mod test {
    use super::super::hasher::Sha256Hasher;
    use super::super::test::*;
    use super::*;

    const BLOCK_SIZE: usize = 1024;

    fn storage(l: usize) -> Result<SizedTestTreeStorage, CreationError> {
        let layout = FullLayout::from(SHA256_STANDARD);
        SizedTestTreeStorage::new_bound(layout, vec![0; l].into())
    }

    #[test]
    fn full_offset_test() {
        let bs = SHA256_STANDARD.block_size() as u64;
        let hs = FullLayout::<Sha256Hasher>::node_size() as u64;

        let layout = FullLayout::from(SHA256_STANDARD);

        assert_eq!(layout.offset_of(&Node::leaf(0)), 0);
        assert_eq!(layout.offset_of(&Node::branch(0)), bs * 1 + hs * 1);
        assert_eq!(layout.offset_of(&Node::leaf(1)), bs * 1 + hs * 2);
        assert_eq!(layout.offset_of(&Node::branch(1)), bs * 2 + hs * 3);
        assert_eq!(layout.offset_of(&Node::branch(2)), bs * 3 + hs * 5);
        assert_eq!(layout.offset_of(&Node::leaf(3)), bs * 3 + hs * 6);
        assert_eq!(layout.offset_of(&Node::branch(4)), bs * 5 + hs * 9);
        assert_eq!(layout.offset_of(&Node::leaf(6)), bs * 6 + hs * 12);
        assert_eq!(layout.offset_of(&Node::leaf(7)), bs * 7 + hs * 14);
        assert_eq!(layout.offset_of(&Node::branch(7)), bs * 8 + hs * 15);
    }

    #[test]
    fn hash_offset_test() {
        let hs = HashOnlyLayout::<Sha256Hasher>::node_size() as u64;

        let layout = HashOnlyLayout::from(SHA256_STANDARD);

        assert_eq!(layout.offset_of(&Node::leaf(0)), 0);
        assert_eq!(layout.offset_of(&Node::branch(0)), hs * 1);
        assert_eq!(layout.offset_of(&Node::leaf(1)), hs * 2);
        assert_eq!(layout.offset_of(&Node::branch(1)), hs * 3);
        assert_eq!(layout.offset_of(&Node::branch(2)), hs * 5);
        assert_eq!(layout.offset_of(&Node::leaf(3)), hs * 6);
        assert_eq!(layout.offset_of(&Node::branch(4)), hs * 9);
        assert_eq!(layout.offset_of(&Node::leaf(6)), hs * 12);
        assert_eq!(layout.offset_of(&Node::leaf(7)), hs * 14);
        assert_eq!(layout.offset_of(&Node::branch(7)), hs * 15);
    }

    #[test]
    fn new() {
        let (bs, hs) = (BLOCK_SIZE, FullLayout::<Sha256Hasher>::node_size());

        fn _leaves_for_size(l: usize) -> Result<usize, CreationError> {
            storage(l).map(|l| l.leaves)
        }

        assert_eq!(_leaves_for_size(0), Ok(0));
        assert_matches!(_leaves_for_size(1), Err(CreationError::InvalidSize));
        assert_matches!(_leaves_for_size(hs), Err(CreationError::InvalidSize));
        assert_eq!(_leaves_for_size(hs + 1), Ok(1));
        assert_eq!(_leaves_for_size(hs + bs), Ok(1));
        assert_matches!(
            _leaves_for_size(hs + bs + 1),
            Err(CreationError::InvalidSize)
        );
        assert_matches!(
            _leaves_for_size(3 * hs + 1 * bs),
            Err(CreationError::InvalidSize)
        );
        assert_eq!(_leaves_for_size(3 * hs + 1 * bs + 1), Ok(2));
        assert_eq!(_leaves_for_size(3 * hs + 2 * bs), Ok(2));
        assert_eq!(_leaves_for_size(5 * hs + 2 * bs + 1), Ok(3));
        assert_eq!(_leaves_for_size(7 * hs + 4 * bs - 1), Ok(4));
    }

    #[test]
    fn storage_required() {
        let layout = FullLayout::from(SHA256_STANDARD);
        let bs = SHA256_STANDARD.block_size() as u64;
        let hs = FullLayout::<Sha256Hasher>::node_size() as u64;

        assert_eq!(layout.storage_required(0 * bs), 0);
        assert_eq!(layout.storage_required(1 * bs), bs * 1 + hs * 1);
        assert_eq!(layout.storage_required(2 * bs), bs * 2 + hs * 3);
        assert_eq!(layout.storage_required(3 * bs), bs * 3 + hs * 5);
        assert_eq!(layout.storage_required(4 * bs), bs * 4 + hs * 7);
    }
}
