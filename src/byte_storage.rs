use std::cmp::min;
use std::os::unix::io::AsRawFd;
use std::{fs, io};

use bytes::{BufMut, Bytes, BytesMut};
use nix::sys::uio;

/// Implements reading and writing byte-slices from storage
pub trait ByteStorage {
    fn read(&self, offset: u64, size: usize) -> Result<Bytes, io::Error>;
    fn write(&mut self, offset: u64, data: &[u8]) -> Result<(), io::Error>;

    fn read_all(&self, offset: u64, size: usize) -> Result<Bytes, io::Error> {
        let res = self.read(offset, size)?;
        if res.len() == size {
            Ok(res)
        } else {
            Err(io::Error::new(
                io::ErrorKind::UnexpectedEof,
                "insufficient data for read",
            ))
        }
    }
}

/// Sub-trait of ByteStorage, allowing writing after end of file
pub trait ExtendableByteStorage: ByteStorage {
    type BoundByteStorage: BoundByteStorage;

    /// Queries current length of underlying storage.
    /// Possibly expensive, requiring fetch from slow media
    fn len(&self) -> u64;
    fn freeze_len(self) -> Self::BoundByteStorage;

    fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

/// Sub-trait of ByteStorage, preventing writes after EOF
pub trait BoundByteStorage: ByteStorage {
    /// Length of the static bound
    /// Cheap, should be a simple struct-lookup
    fn len(&self) -> u64;

    fn is_empty(&self) -> bool {
        self.len() == 0
    }

    fn validate_end(&self, offset: u64, len: usize) -> Result<u64, io::Error> {
        let end = offset + len as u64;
        if end <= self.len() {
            Ok(end)
        } else {
            Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                "Access beyond EOF",
            ))
        }
    }
}

impl ByteStorage for Vec<u8> {
    fn read(&self, offset: u64, size: usize) -> Result<Bytes, io::Error> {
        let self_len = (self as &Vec<u8>).len();
        if offset < self_len as u64 {
            let o = offset as usize;
            let end = min(o + size, self_len);
            Ok(Bytes::from(&self[o..end]))
        } else {
            Ok(Bytes::from(&[][..]))
        }
    }
    fn write(&mut self, offset: u64, data: &[u8]) -> Result<(), io::Error> {
        let o = offset as usize;
        let end = o + data.len();
        if (self as &Vec<u8>).len() < end {
            self.resize(end, 0u8);
        }
        self[o..o + data.len()].copy_from_slice(data);
        Ok(())
    }
}

impl ExtendableByteStorage for Vec<u8> {
    type BoundByteStorage = Box<[u8]>;

    fn len(&self) -> u64 {
        (self as &Vec<u8>).len() as u64
    }

    fn freeze_len(self) -> Self::BoundByteStorage {
        self.into()
    }
}

impl ByteStorage for Box<[u8]> {
    fn read(&self, offset: u64, size: usize) -> Result<Bytes, io::Error> {
        let o = offset as usize;
        if offset < (self as &BoundByteStorage).len() {
            let end = min(o + size, self.as_ref().len());
            Ok(Bytes::from(&self[o..end]))
        } else {
            Ok(Bytes::from(&[][..]))
        }
    }
    fn write(&mut self, offset: u64, data: &[u8]) -> Result<(), io::Error> {
        let end = self.validate_end(offset, data.len())?;
        self[offset as usize..end as usize].copy_from_slice(data);
        Ok(())
    }
}

impl BoundByteStorage for Box<[u8]> {
    fn len(&self) -> u64 {
        self.as_ref().len() as u64
    }
}

impl ByteStorage for fs::File {
    fn read(&self, offset: u64, size: usize) -> Result<Bytes, io::Error> {
        let mut res = BytesMut::with_capacity(size);
        match uio::pread(self.as_raw_fd(), unsafe { res.bytes_mut() }, offset as i64) {
            Ok(read) => {
                unsafe {
                    res.advance_mut(read);
                }
                Ok(res.freeze())
            }
            Err(e) => Err(io::Error::new(io::ErrorKind::Other, e)),
        }
    }
    fn write(&mut self, offset: u64, data: &[u8]) -> Result<(), io::Error> {
        match uio::pwrite(self.as_raw_fd(), data, offset as i64) {
            Ok(written) if written == data.len() => Ok(()),
            Ok(_) => Err(io::Error::new(
                io::ErrorKind::Other,
                "Underlying layer did not write all data",
            )),
            Err(e) => Err(io::Error::new(io::ErrorKind::Other, e)),
        }
    }
}

impl ExtendableByteStorage for fs::File {
    type BoundByteStorage = AssetFile;

    fn len(&self) -> u64 {
        self.metadata().expect("Metadata Readable").len()
    }

    fn freeze_len(self) -> Self::BoundByteStorage {
        self.into()
    }
}

pub struct AssetFile {
    file: fs::File,
    len: u64,
}

impl BoundByteStorage for AssetFile {
    fn len(&self) -> u64 {
        self.len
    }
}

impl From<fs::File> for AssetFile {
    fn from(f: fs::File) -> AssetFile {
        AssetFile {
            len: f.metadata().expect("Readable").len(),
            file: f,
        }
    }
}

impl ByteStorage for AssetFile {
    fn read(&self, offset: u64, size: usize) -> Result<Bytes, io::Error> {
        self.file.read(offset, size)
    }
    fn write(&mut self, offset: u64, data: &[u8]) -> Result<(), io::Error> {
        self.validate_end(offset, data.len())?;
        self.file.write(offset, data)
    }
}
