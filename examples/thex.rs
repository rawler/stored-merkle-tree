extern crate stored_merkle_tree;

use std::io::{stdin, Error};
use stored_merkle_tree::{HashOnlyLayout, SHA256_STANDARD, SimpleSetup};

fn main() -> Result<(), Error> {
    let stdin = stdin();
    let builder = HashOnlyLayout::make_builder(&SHA256_STANDARD, Vec::new());
    let tree = builder.consume_and_finish(stdin.lock())?;

    println!("{:x}", tree.root_hash().expect("Readable"));
    Ok(())
}
