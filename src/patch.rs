use super::error::*;
use super::hasher::Hasher;
use super::index::{Index, Node};
use super::io::BlockSlice;
use super::model::NodeContent;
use super::tree::TreeLayout;

use std::collections::btree_map::{BTreeMap, Entry};
use std::fmt::{self, Debug};
use std::iter::FromIterator;

#[derive(Debug, Clone)]
pub(crate) struct Proof<H: Hasher>(BTreeMap<Node, NodeContent<H::Output>>);

impl<H: Hasher> Proof<H> {
    fn add(&mut self, n: Node, h: NodeContent<H::Output>) -> Result<(), PatchError> {
        if let Some(v) = self.0.insert(n, h) {
            if h.hash != v.hash {
                return Err(PatchError::Internal(n));
            }
        }
        Ok(())
    }

    pub(crate) fn with_data(self, data: BlockSlice) -> Patch<H> {
        Patch { data, proof: self }
    }
}

impl<H: Hasher> FromIterator<(Node, H::Output)> for Proof<H> {
    fn from_iter<T>(i: T) -> Self
    where
        T: IntoIterator<Item = (Node, H::Output)>,
    {
        Proof(
            i.into_iter()
                .map(|(n, h)| (n, NodeContent::rumored(&h)))
                .collect(),
        )
    }
}

impl<H: Hasher> IntoIterator for Proof<H> {
    type Item = <BTreeMap<Node, NodeContent<H::Output>> as IntoIterator>::Item;
    type IntoIter = <BTreeMap<Node, NodeContent<H::Output>> as IntoIterator>::IntoIter;
    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

/// Every node in this tree is proven towards one of the root-nodes
pub struct VerifiedProof<H: Hasher> {
    nodes: BTreeMap<Node, NodeContent<H::Output>>,
    roots: Vec<Node>,
}

impl<H: Hasher> VerifiedProof<H> {
    fn new<Iter, Tree>(nodes: Iter, hasher: &H, tree: &Tree) -> Result<Self, PatchError>
    where
        Iter: Iterator<Item = (Node, NodeContent<H::Output>)>,
        Tree: TreeLayout,
    {
        let mut res = Self {
            nodes: BTreeMap::new(),
            roots: Vec::new(),
        };
        for (idx, content) in nodes {
            res.add_propagate(&idx, content, hasher, tree)?;
        }

        res.roots = res
            .nodes
            .keys()
            .filter(|&idx| match tree.effective_parent(idx) {
                Some(parent) => !res.nodes.contains_key(&parent.to_node()),
                None => true,
            })
            .cloned()
            .collect();

        Ok(res)
    }

    fn add(&mut self, idx: &Node, content: NodeContent<H::Output>) -> Result<bool, PatchError> {
        match self.nodes.entry(*idx) {
            Entry::Vacant(e) => {
                e.insert(content);
                Ok(true)
            }
            Entry::Occupied(mut e) => if content
                .is_update_over(Some(e.get()))
                .map_err(|(_, _)| PatchError::Internal(*idx))?
            {
                e.insert(content);
                Ok(true)
            } else {
                Ok(false)
            },
        }
    }

    fn add_propagate<Tree: TreeLayout>(
        &mut self,
        idx: &Node,
        content: NodeContent<H::Output>,
        hasher: &H,
        tree: &Tree,
    ) -> Result<(), PatchError> {
        if !self.add(idx, content)? {
            return Ok(());
        }
        let (sibling, parent) = match tree.effective_sibling_and_parent(&idx) {
            Some(node) => node,
            None => return Ok(()),
        };
        let parent_content = match self.nodes.get(&sibling) {
            Some(sibling_content) => {
                hasher.merge_tree_unsorted((&sibling, sibling_content), (idx, &content))
            }
            None => return Ok(()),
        };
        self.add_propagate(&parent.to_node(), parent_content, hasher, tree)
    }

    pub fn roots(&self) -> impl Iterator<Item = (&Node, &NodeContent<H::Output>)> {
        self.roots.iter().map(move |idx| (idx, &self.nodes[idx]))
    }

    pub fn iter(&self) -> impl Iterator<Item = (&Node, &NodeContent<H::Output>)> {
        self.nodes.iter()
    }

    pub fn is_filled(&self, idx: Node) -> bool {
        match self.nodes.get(&idx) {
            Some(NodeContent {
                is_filled: filled, ..
            }) => *filled,
            None => false,
        }
    }
}

/// A slice of data, combined with a proof of hashes
///
/// Patches can only be valid in the context of the right [HashTree](struct.HashTree.html)
pub struct Patch<H: Hasher> {
    pub(crate) data: BlockSlice,
    pub(crate) proof: Proof<H>,
}

impl<H: Hasher> Debug for Patch<H> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(
            fmt,
            "Patch {{ data: {:?}, proof: {:?} }}",
            self.data.leaves(),
            self.proof.0.keys()
        )
    }
}

impl<H: Hasher> Patch<H> {
    pub fn new<ProofIter>(data: BlockSlice, proof: ProofIter) -> Self
    where
        ProofIter: IntoIterator<Item = (Node, H::Output)>,
    {
        Self {
            data: data,
            proof: Proof::from_iter(proof),
        }
    }

    pub(crate) fn inflate<Tree: TreeLayout>(
        mut self,
        hasher: &H,
        tree: &Tree,
    ) -> Result<VerifiedPatch<H>, PatchError> {
        self.inflate_leaf_hashes(hasher)?;
        let proof = VerifiedProof::new(self.proof.0.into_iter(), hasher, tree)?;
        Ok(VerifiedPatch {
            data: self.data,
            proof: proof,
        })
    }

    pub fn data_blocks(&self) -> &BlockSlice {
        &self.data
    }

    pub fn proof(&self) -> impl Iterator<Item = (&Node, &H::Output)> {
        self.proof
            .0
            .iter()
            .map(|(idx, content)| (idx, &content.hash))
    }

    fn inflate_leaf_hashes(&mut self, hasher: &H) -> Result<(), PatchError> {
        for (i, b) in self.data.iter() {
            self.proof.add(Node::leaf(i), hasher.leaf_node(b))?;
        }
        Ok(())
    }
}

pub(crate) struct VerifiedPatch<H: Hasher> {
    pub data: BlockSlice,
    pub proof: VerifiedProof<H>,
}

impl<H: Hasher> Debug for VerifiedPatch<H> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        let proof: Vec<_> = self
            .proof
            .nodes
            .iter()
            .map(|(&idx, &node)| (idx, node.is_filled))
            .collect();
        write!(
            fmt,
            "VerifiedPatch {{ data: {:?}, proof: {:?} }}",
            self.data.leaves(),
            proof,
        )
    }
}

#[cfg(test)]
mod test {
    use std::ops::Range;

    use super::super::flatten::{FullLayout, SimpleSetup, TreeLayout};
    use super::super::hasher::Sha256Hasher;
    use super::super::index::Leaf;
    use super::super::model::{ExtendableNodeWriter, HashReader, SizedTree};
    use super::super::test::*;
    use super::super::HashTree;
    use super::*;

    use proptest::collection::btree_set;
    use proptest::prelude::*;

    type HashValue = <Sha256Hasher as Hasher>::Output;

    fn mk_patch(data: BlockSlice, hashes: Vec<(Node, HashValue)>) -> Patch<Sha256Hasher> {
        Patch {
            data: data,
            proof: hashes.into_iter().collect(),
        }
    }

    fn ancestor<I: Index>(x: I, level: usize) -> Node {
        let mut res = x.to_node();
        for _ in 0..level {
            res = res.parent().to_node()
        }
        res
    }

    struct RefTree(HashTree<Sha256Hasher, <TestTreeStorage as ExtendableNodeWriter>::Sized>);

    impl RefTree {
        fn new(data: &[u8]) -> RefTree {
            let tree = FullLayout::make_builder(&SHA256_STANDARD, Vec::new())
                .consume_and_finish(data)
                .expect("Built OK");
            RefTree(tree)
        }

        fn pick_hashes(&self, indices: &[Node]) -> Vec<(Node, HashValue)> {
            indices
                .iter()
                .map(|idx| (*idx, self.0.storage.read_hash(idx).unwrap().unwrap().hash))
                .collect()
        }

        fn patch_for(&self, leaves: Range<Leaf>) -> Result<Patch<Sha256Hasher>, TreeError> {
            self.0.patch_for(
                leaves,
                Leaf(0)..self.0.storage.tree().last_leaf().unwrap().next(),
            )
        }

        fn root(&self) -> HashValue {
            self.root_node().hash
        }

        fn root_node(&self) -> NodeContent<HashValue> {
            self.0.root_hash().unwrap()
        }
    }

    #[test]
    fn new() {
        let layout = FullLayout::from(SHA256_STANDARD);
        let size = layout.storage_required(SHA256_STANDARD.block_size() as u64 * 1025);
        let storage =
            SizedTestTreeStorage::new_bound(layout, vec![0u8; size as usize].into()).unwrap();
        let root_hash = HashValue::from_slice(&hex!(
            "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
        ));
        let mut base = HashTree::new_for(&SHA256_STANDARD, storage, root_hash).expect("Created OK");

        assert_eq!(
            base.merge(mk_patch(
                SHA256_STANDARD.split([][..].into()).with_offset(0),
                vec![]
            )),
            Ok(())
        );
        assert_eq!(
            base.merge(mk_patch(
                SHA256_STANDARD
                    .split(vec![0u8; 1024].into())
                    .with_offset(16),
                vec![]
            )),
            Err(TreeError::InvalidPatch(PatchError::Attachment(Node::Leaf(
                16,
            ))))
        );
    }

    #[test]
    fn valid_use() {
        let reference = RefTree::new(&[0u8; 1024 * 32]);

        let mut base_storage =
            TestTreeStorage::new_empty(FullLayout::from(SHA256_STANDARD), Vec::new());
        base_storage
            .write_leaf(
                &Leaf(31),
                &SHA256_STANDARD.leaf_node(&[0u8; 1024]).hash,
                &[0u8; 1024],
            )
            .expect("Success");

        let mut base = HashTree::new_for(
            &SHA256_STANDARD,
            base_storage.try_into_sized().unwrap(),
            &reference.root(),
        ).expect("Created OK");

        assert_eq!(
            base.merge(mk_patch(
                SHA256_STANDARD.split([][..].into()).with_offset(0),
                vec![]
            )),
            Ok(())
        );
        assert_eq!(
            base.merge(mk_patch(
                SHA256_STANDARD
                    .split(vec![0u8; 1024].into())
                    .with_offset(16),
                reference.pick_hashes(&[
                    ancestor(Node::leaf(16), 0).sibling(),
                    ancestor(Node::leaf(16), 1).sibling(),
                    ancestor(Node::leaf(16), 2).sibling(),
                    ancestor(Node::leaf(16), 3).sibling(),
                    ancestor(Node::leaf(16), 4).sibling(),
                ])
            )),
            Ok(())
        );

        assert_eq!(
            base.storage.read_hash(&Node::branch(16)).expect("readable"),
            Some(NodeContent::rumored(HashValue::from_slice(&hex!(
                "45052bc018310b1717b17f3a2965c25f4f2e5c88ea4a86a5d9bdbee5542a782a"
            ))))
        );

        assert_eq!(
            base.merge(mk_patch(
                SHA256_STANDARD
                    .split(vec![0u8; 1024].into())
                    .with_offset(17),
                vec![]
            )),
            Ok(())
        );
        assert_eq!(
            base.storage.read_hash(&Node::branch(16)).expect("readable"),
            Some(NodeContent::filled(HashValue::from_slice(&hex!(
                "45052bc018310b1717b17f3a2965c25f4f2e5c88ea4a86a5d9bdbee5542a782a"
            ))))
        );
    }

    #[test]
    fn test_small_tree() {
        let whole_blocks = 2;
        let data_size = SHA256_STANDARD.block_size() * whole_blocks + 1;
        check_replication(data_size, vec![0..1, 1..whole_blocks + 1])
    }

    fn check_replication(data_size: usize, patches: Vec<Range<usize>>) {
        let reference = RefTree::new(&vec![0u8; data_size]);
        let mut target = TreeBuilder::new(data_size)
            .with_root(reference.root())
            .build();

        for patch in patches {
            let patch = reference
                .patch_for(Leaf::range(patch))
                .expect("Valid patch");
            target.merge(patch).expect("Working merge");
        }
        assert_eq!(target.root_hash().unwrap(), reference.root_node());
    }

    fn arbitary_patches(max_blocks: usize) -> BoxedStrategy<Vec<Range<usize>>> {
        (1..max_blocks)
            .prop_flat_map(|block_count| {
                btree_set(1..block_count, 0..block_count).prop_map(move |splits| {
                    let mut res = Vec::new();
                    let mut start = 0;
                    for split in splits.into_iter().chain(Some(block_count)) {
                        res.push(start..split);
                        start = split;
                    }
                    res
                })
            })
            .prop_shuffle()
            .boxed()
    }

    proptest! {
        #[test]
        fn test_valid_uses(ref patches in arbitary_patches(64), last_block in 1..1024usize) {
            let whole_blocks = patches.iter().map(|range|range.end).max().unwrap()-1;
            let data_size = SHA256_STANDARD.block_size() * whole_blocks + last_block;
            check_replication(data_size, patches.to_vec())
        }
    }
}
