#[macro_use]
extern crate criterion;

extern crate bytes;
extern crate stored_merkle_tree;

use criterion::Criterion;

use stored_merkle_tree::hasher::Sha256Hasher;
use stored_merkle_tree::index::Leaf;
use stored_merkle_tree::{flatten, flatten::TreeLayout, Builder, HashTree, SHA256_STANDARD};

type TS = flatten::TreeStorage<flatten::FullLayout<Sha256Hasher>, Box<[u8]>>;

fn build() -> (HashTree<Sha256Hasher, TS>, HashTree<Sha256Hasher, TS>) {
    let data_len = 1024 * 1237;
    let layout = flatten::FullLayout::from(SHA256_STANDARD);

    let reference = Builder::new(&SHA256_STANDARD, layout.initialize_empty(Vec::new()))
        .consume_and_finish(vec![0u8; data_len].as_slice())
        .expect("Ok");

    let root_hash = &reference.root_hash().unwrap().hash;

    let target_size = layout.storage_required(data_len as u64) as usize;
    let target = layout
        .wrap(vec![0u8; target_size].into_boxed_slice())
        .unwrap();

    (
        reference,
        HashTree::new_for(&SHA256_STANDARD, target, &root_hash).unwrap(),
    )
}

fn patch(reference: HashTree<Sha256Hasher, TS>, mut target: HashTree<Sha256Hasher, TS>) {
    let last_leaf = reference.tree().last_leaf().unwrap();
    for leaf in [1].iter() {
        let start = Leaf(*leaf);
        let stop = Leaf(leaf + 1);
        let patch = reference
            .patch_for(start..stop, Leaf(0)..last_leaf.next())
            .expect("Valid patch");
        target.merge(patch).expect("Working merge");
    }
}

fn thex_patch_benchmark(c: &mut Criterion) {
    c.bench_function("build", |b| b.iter(|| build()));
    let (reference, target) = build();
    let reference = reference.unwrap_storage();
    let target = target.unwrap_storage();
    c.bench_function("patch", |b| {
        b.iter(|| {
            let reference = HashTree::new(&SHA256_STANDARD, reference.clone()).unwrap();
            let target = HashTree::new(&SHA256_STANDARD, target.clone()).unwrap();

            patch(reference, target)
        })
    });
}

criterion_group!(benches, thex_patch_benchmark);
criterion_main!(benches);
